//! bare3.rs
//!
//! String types in Rust
//!
//! What it covers:
//! - Types, str, arrays ([u8; usize]), slices (&[u8])
//! - Iteration, copy
//! - Semihosting (tracing using `hprintln`

    #![no_main]
    #![no_std]
    
    extern crate panic_halt;
    
    use cortex_m_rt::entry;
    use cortex_m_semihosting::{hprint, hprintln};
    
    
    
    #[entry]
    fn main() -> ! {
        hprintln!("bare3").unwrap();
        let s  =  "ABCD"; 
        let bs: &[u8] = s.as_bytes();
    
        hprintln!("s = {}", s).unwrap();
        hprintln!("bs = {:?}", bs).unwrap();
    
        hprintln!("iterate over slice").unwrap();
        // implementation
        let c: &[u8];   
        // loop trough every bytes in the array bytes
        for c in bs {
            hprint!("{},", c).unwrap();//display the value of c
        }
    
        let k: usize ;// init
        hprintln!("iterate iterate using (raw) indexing").unwrap();
        for k in 0..s.len() {
            hprintln!("{},", bs[k]).unwrap();
        }
        hprintln!("").unwrap();
         //let a = [65u8; 4];
         //let mut a = [0u8; 4];
         // implementation
         let a = &mut [0,1,2,3];
         // loop through 
         for j in 0..bs.len() {
             a[j] = bs[j];// a of index j takes the value of bs of index j
            hprint!("{},", a).unwrap();//display of a in the loop
        }
        hprintln!("").unwrap();
    //hprintln!("a = {}", core::str::from_utf8(&a).unwrap()).unwrap();
    // loop
    for i in 0..a.len() {
        hprintln!("{},", a[i]).unwrap();
    }
    let x = bs;  // x takes the value of bs 
    
    hprintln!("writing the x").unwrap();
    //loop 
    for i in 0..a.len() {
        // display 
        hprintln!("{},", x[i]).unwrap();
    }
    
        loop {
            continue;
        }
    }
    
    // 0. Build and run the application (debug build).
    //
    //    > cargo run --example bare3
    //    (or use the vscode build task)
    // **Done**
    // 1. What is the output in the `openocd` (Adapter Output) console?
    //   **The output was :**
    //    ** bare3
    // s = ABCD
    // bs = [65, 66, 67, 68]
    // iterate over slice
    // 65,66,67,68,iterate iterate using (raw) indexing
    // 65,
    // 66,
    // 67,
    // 68,
    // a = AAAA **
    //
    //    What is the type of `s`?
    //
    //    ** The type of s is string or as know as str for programmer**
    //
    //    What is the type of `bs`?
    //
    //    ** bs refers to the type array bytes in programming **
    //
    //    What is the type of `c`?
    //
    //    ** c refers to the byte type in programming**
    //
    //    What is the type of `a`?
    //
    //    ** a is a set of 4 bytes  **
    //
    //    What is the type of `i`?
    //
    //    ** i is an int that refers to integers more precisely**
    //
    //    Commit your answers (bare3_1)
    //
    // 2. Make types of `s`, `bs`, `c`, `a`, `i` explicit.
    //
    //    Commit your answers (bare3_2)
    //    **Done*
    // 3. Uncomment line `let mut a = [0u8; 4];
    //`
    //    Run the program, what happens and why?
    //    ** We can observe that nothing is displayed/printed. It is because if we refer to the table ASCII for type and character
            // We can see that 0u8 = 0 is equivalent to Null in the table so nothing.
    //    
    //
    //    Commit your answers (bare3_3)
    //
    // 4. Alter the program so that the data from `bs` is copied byte
    //    by byte into `a` using a loop and raw indexing.
    //
    //    Test that it works as intended.
    //     **Done**
    //    Commit your answers (bare3_4)
    //
    // 5. Look for a way to make this copy done without a loop.
    //    https://doc.rust-lang.org/std/primitive.slice.html
    //
    //    Implement and test your solution.
    // **Done**
    //    Commit your answers (bare3_5)
    //
    // 6. Optional
    //    Rust is heavily influenced by functional languages.
    //    Figure out how you can use an iterator to work over both
    //    the `a` and `bs` to copy the content of `bs` to `a`.
    //
    //    You may use
    //    - `iter` (to turn a slice into an iterator)
    //    - `zip` (to merge two slices into an iterator)
    //    - a for loop to assign the elements
    //
    //    Commit your solution (bare3_6)
    //
    // 7. Optional
    //    Iter using `foreach` and a closure instead of the for loop.
    //
    //    Commit your solution (bare3_7)
    //
    // 8. Optional*
    //    Now benchmark your different solutions using the cycle accurate
    //    DWT based approach (in release mode).
    //
    //    Cycle count for `raw` indexing
    //
    //    ** your answer here **
    //
    //    Cycle count for the primitive slice approach.
    //
    //    ** your answer here **
    //
    //    Cycle count for the primitive slice approach.
    //
    //    ** your answer here **
    //
    //    Cycle count for the zip + for loop approach.
    //
    //    ** your answer here **
    //
    //    Cycle count for the zip + for_each approach.
    //
    //    What conclusions can you draw, does Rust give you zero-cost abstractions?
    //
    //    ** your answer here **
    