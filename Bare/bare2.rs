//! bare2.rs
//!
//! Measuring execution time
//!
//! What it covers
//! - Generating documentation
//! - Using core peripherals
//! - Measuring time using the DWT
//! - ITM tracing using `iprintln`
//! - Panic halt
//!

#![no_main]
#![no_std]

use panic_halt as _;

use cortex_m::{iprintln, peripheral::DWT, Peripherals};
use cortex_m_rt::entry;

// burns CPU cycles by just looping `i` times
#[inline(never)]
fn wait(i: u32) {
    for _ in 0..i {
        // no operation (ensured not optimized out)
        cortex_m::asm::nop();
    }
}

#[entry]
fn main() -> ! {
    let mut p = Peripherals::take().unwrap();
    let stim = &mut p.ITM.stim[0];
    let mut dwt = p.DWT;

    iprintln!(stim, "bare2");

    dwt.enable_cycle_counter();

    // Reading the cycle counter can be done without `owning` access
    // the DWT (since it has no side effect).
    //
    // Look in the docs:
    // pub fn enable_cycle_counter(&mut self)
    // pub fn get_cycle_count() -> u32
    //
    // Notice the difference in the function signature!

    let start = DWT::get_cycle_count();
    wait(1_000_000);
    let end = DWT::get_cycle_count();

    // notice all printing outside of the section to measure!
    iprintln!(stim, "Start {:?}", start);
    iprintln!(stim, "End {:?}", end);
    iprintln!(stim, "Diff {:?}", end - start);

    loop {}
}

// 0. Setup
//    > cargo doc --open
//
//    This will document your crate, and open the docs in your browser.
//    If it does not auto-open, then copy paste the path in your browser.
//    (Notice, it will try to document all dependencies, you may have only one
//    one panic handler, so comment out all but one in `Cargo.toml`.)
//
//    In the docs, search (`S`) for DWT, and click `cortex_m::peripheral::DWT`.
//    Read the API docs.
//
// 1. Build and run the application (debug build).
//    Setup ITM tracing (see `bare1.rs`) and `openocd` (if not using vscode).
//
//    > cargo run --example bare2
//    (or use the vscode build task)
//     **Done*
//    What is the output in the ITM console?
//   The output of the console is : 

//*    ** 0x08000804 <+0>:	push	{r4, lr}
//    0x08000806 <+2>:	sub	sp, #8
//    0x08000808 <+4>:	movw	r1, #16960	; 0x4240
//    0x0800080c <+8>:	movt	r1, #15
//    0x08000810 <+12>:	movs	r0, #0
//    0x08000812 <+14>:	bl	0x80008fc <<I as core::iter::traits::collect::IntoIterator>::into_iter>
//    0x08000816 <+18>:	strd	r0, r1, [sp]
//    0x0800081a <+22>:	mov	r0, sp
//    0x0800081c <+24>:	bl	0x80006ba <core::iter::range::<impl core::iter::traits::iterator::Iterator for core::ops::range::Range<A>>::next>
//    0x08000820 <+28>:	cbz	r0, 0x8000832 <bare2::wait+46>
//    0x08000822 <+30>:	mov	r4, sp
//    0x08000824 <+32>:	bl	0x800077e <cortex_m::asm::nop>
//    0x08000828 <+36>:	mov	r0, r4
//    0x0800082a <+38>:	bl	0x80006ba <core::iter::range::<impl core::iter::traits::iterator::Iterator for core::ops::range::Range<A>>::next>
// => 0x0800082e <+42>:	cmp	r0, #0
//    0x08000830 <+44>:	bne.n	0x8000824 <bare2::wait+32>
//    0x08000832 <+46>:	add	sp, #8
//    0x08000834 <+48>:	pop	{r4, pc} **
//
//    Rebuild and run in release mode
//
//    > cargo build --example bare2 --release
//   **Done** :

//       0x08000c12 <+0>:	push	{r7, lr}
//       0x08000c14 <+2>:	bl	0x80009cc <core::cell::UnsafeCell<T>::get>
//    => 0x08000c18 <+6>:	bl	0x8000954 <core::ptr::read_volatile>
//       0x08000c1c <+10>:	subs	r0, #1
//       0x08000c1e <+12>:	clz	r0, r0
//       0x08000c22 <+16>:	lsrs	r0, r0, #5
//       0x08000c24 <+18>:	pop	{r7, pc}
//
//    Compute the ratio between debug/release optimized code
//    (the speedup).
//
//    ** I obtained this ratio : 50/21 = 2,38 I did it few times and I obtained almost the same ratio. But when I had MATLAB open that was longer :D*
//
//    commit your answers (bare2_1)
//
// 3. *Optional
//    Inspect the generated binaries, and try stepping through the code
//    for both debug and release binaries. How do they differ?
//
//    ** your answer here **
//
//    commit your answers (bare2_2)
